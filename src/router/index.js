import Vue from "vue";
import VueRouter from "vue-router";
import ToS from '../components/tos/ToS.vue';
import Privacy from '../components/privacy/Privacy.vue';
import General from '../containers/general/General.vue';

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: General
    },
    {
      path: "/tos",
      component: ToS
    },
    {
      path: "/privacy",
      component: Privacy
    }
  ]
});
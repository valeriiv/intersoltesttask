module.exports = {
    devServer: {
        compress: true,
        hot: true,
        port: 8090,
        open: true
    }
};
# intersoltesttask

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Сверстать вот такую страницу по примеру (ниже). При разработке использовать vue
1. Это встраиваемый виджет в сайт, должен быть отцентрован по середине
2. Должен адаптироваться под мобильные телефоны
3. максимальный размер 720*540
4. код разбить на компоненты
5. основной упор нужно сделать  на вёрстку

Вариант отдачи - по желанию, или залить на хост или передать локально



[Link to Figma](https://www.figma.com/file/UD1bytuaI4REE8sWPppeCssA/test_section?node-id=0%3A1)